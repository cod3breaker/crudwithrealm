//
//  UpdateVC.swift
//  CrudWithRealm
//
//  Created by Sunimal Herath on 6/7/19.
//  Copyright © 2019 Sunimal Herath. All rights reserved.
//

import UIKit
import RealmSwift

class UpdateVC: UIViewController {

    let realm = try! Realm()
    
    @IBOutlet weak var textToUpdate: UITextField!
    
    var crudArray: Results<Crud>?
    var crudToUpdate: Crud?
    
    var passingIndex: Int?
    
//    var passingIndex: Int? {
//        didSet{
//            loadCrud()
//        }
//    }
//    THIS BLOODY THING RETURNEND AN ERROR!!! 
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadCrud()
    }
    
    func loadCrud(){
        crudArray = realm.objects(Crud.self)
        
        if let indexToUse = passingIndex{
            crudToUpdate = crudArray?[indexToUse]
            textToUpdate.text = crudToUpdate?.title
        }
    }
    
    @IBAction func updateCrudButtonPressed(_ sender: Any) {
        
        do{
            try realm.write {
                crudToUpdate?.title = textToUpdate.text!
            }
        } catch {
            print("Error updating value: \(error)")
        }
        
        textToUpdate.text = ""
        
        // Navigate back to the ReadVC
        navigationController?.popViewController(animated: true)
    }
}
