//
//  ReadTableVC.swift
//  CrudWithRealm
//
//  Created by Sunimal Herath on 6/7/19.
//  Copyright © 2019 Sunimal Herath. All rights reserved.
//

import UIKit
import RealmSwift
import SwipeCellKit

class ReadTableVC: UITableViewController {

    let realm = try! Realm()
    var crudArray: Results<Crud>?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //loadTableData()
        tableView.rowHeight = 80.0
    }
    
    override func viewWillAppear(_ animated: Bool) {
        loadTableData()
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return crudArray?.count ?? 1
    }
    
    // MARK: - Table view delegate
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "readCell", for: indexPath) as! SwipeTableViewCell
        
        cell.textLabel?.text = crudArray?[indexPath.row].title ?? "Nothing here"
        cell.delegate = self
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("Selected row: \(indexPath.row)")
        performSegue(withIdentifier: "toUpdateVC", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let selectedIndexPath = tableView.indexPathForSelectedRow {
            let selectedCrud = crudArray?[selectedIndexPath.row]
            print("Selected Crud Obj Title: \(String(describing: selectedCrud?.title))")
            let destinationVC = segue.destination as! UpdateVC
            //destinationVC.passingCrud = selectedCrud
            destinationVC.passingIndex = selectedIndexPath.row
        }
    }
    
    
    // MARK: - Data manipulation functions

    
    func loadTableData(){
        crudArray = realm.objects(Crud.self)
        tableView.reloadData()
    }
}

// MARK: - SwipeTableView Methods

extension ReadTableVC : SwipeTableViewCellDelegate {
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        guard orientation == .right else { return nil }
        
        let deleteAction = SwipeAction(style: .destructive, title: "Delete") { action, indexPath in
            // handle action by updating model with deletion
            if let crudObjectToDelete = self.crudArray?[indexPath.row] {
                do{
                    try self.realm.write {
                        self.realm.delete(crudObjectToDelete)
                    }
                } catch {
                    print("Error deleting data: \(error)")
                }
            }
            //realm.delete(crudArray?[indexPath.row])
        }
        
        // customize the action appearance
        deleteAction.image = UIImage(named: "delete-icon")
        
        return [deleteAction]
    }
    
    func tableView(_ tableView: UITableView, editActionsOptionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> SwipeOptions {
        var options = SwipeOptions()
        options.expansionStyle = .destructive
        options.transitionStyle = .border
        return options
    }
}
