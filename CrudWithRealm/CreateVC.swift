//
//  CreateVC.swift
//  CrudWithRealm
//
//  Created by Sunimal Herath on 6/7/19.
//  Copyright © 2019 Sunimal Herath. All rights reserved.
//

import UIKit
import RealmSwift

class CreateVC: UIViewController {

    let realm = try! Realm()
    
    @IBOutlet weak var textToAddTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // print(Realm.Configuration.defaultConfiguration.fileURL) <= Location of the Realm object (database)
    }
    
    // MARK: - UI Methods
    
    @IBAction func createButtonPressed(_ sender: Any) {
        // Assign textfield value to the title (its property) of the crud object
        let crud = Crud()
        if let textFieldValue = textToAddTextField.text {
            crud.title = textFieldValue
        }
        else{
            crud.title = "Nothing typed. Your fault!"
        }
        
        save(data: crud)
        
        // segue to the ReadTableVC
        performSegue(withIdentifier: "toReadTableVC", sender: self)
        
        // clear textField
        textToAddTextField.text = ""
    }
    
    @IBAction func browseReadVCButtonPressed(_ sender: Any) {
        performSegue(withIdentifier: "toReadTableVC", sender: self)
    }
    
    
    // MARK: - Data manipulation functions
    
    func save(data crud: Crud){
        do{
            try realm.write {
                realm.add(crud)
                //crudArray.append(crud)
            }
        } catch {
            print("Error saving data: \(error)")
        }
    }
}

